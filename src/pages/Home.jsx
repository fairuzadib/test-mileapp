import React from "react";
import Map, {Marker} from 'react-map-gl';

const locations = [
	{
		longitude: 106.816666,
		latitude: -6.200000,
		name: 'Jakarta',
		description: 'Hello from Jakarta'
	},
	{
		longitude: 106.830711,
		latitude:  -6.385589,
		name: 'Depok',
		description: 'Hello from Depok'
	},
	{
		longitude: 106.99542304393111,
		latitude:  -6.241850938105145,
		name: 'Bekasi',
		description: 'Hello from Bekasi'
	},
	{
		longitude: 106.816666,
		latitude: -6.595038,
		name: 'Bogor',
		description: 'Hello from Bogor'
	},
	{
		longitude: 107.30461533621121,
		latitude: -6.310541183712999,
		name: 'Karawang',
		description: 'Hello from Karawang'
	},
]

const Home = () => {
	return (
		<div className="h-screen">
			<Map
				onClick={(e) => console.log(e)}
				initialViewState={{
					longitude: 106.816666,
					latitude: -6.200000,
					zoom: 8.8
				}}
				mapStyle="mapbox://styles/mapbox/streets-v9"
				mapboxAccessToken="pk.eyJ1Ijoic2FoaWx0aGFrYXJlNTIxIiwiYSI6ImNrbjVvMTkzNDA2MXQydnM2OHJ6aHJvbXEifQ.z5aEqRBTtDMWoxVzf3aGsg"
			>
				{
					locations.map(item => (
						<Marker key={item.name} longitude={item.longitude} latitude={item.latitude} anchor="bottom" >
							<div className="border border-gray-200 rounded-md bg-white px-3 py-2">
								<div className="font-semibold text-base">{item.name}</div>
								<div className="text-sm">{item.description}</div>
							</div>
							<img src="https://img.icons8.com/color/48/000000/place-marker--v1.png"/>
						</Marker>
					))
				}
			</Map>
		</div>
	)
}

export default Home
