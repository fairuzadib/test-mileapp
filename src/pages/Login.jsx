import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'

const Login = () => {
	const navigate = useNavigate()
	const [userName, setUserName] = useState('admin')
	const [password, setPassword] = useState('mileapp')
	const [orgName, setOrgName] = useState('Test')
	const [showPassword, setShowPassword] = useState(false)

	const handleLogin = (e) => {
		e.preventDefault()
		e.stopPropagation()
		if (userName === 'admin' && password === 'mileapp') {
			navigate('/')
		} else {
			alert('email or password is invalid')
		}
	}

	return (
		<div className="mt-60 flex justify-center">
			<div>
				<div className="flex bg-white w-full rounded-lg py-8 px-10 shadow-2xl">
					<div>
						<div className="mb-6">
							<h2 className="text-xl font-bold mb-1">
								Login Account
							</h2>
							<p className="text-gray-500">
								Please sign-in to your account
							</p>
						</div> <span>
							<form onSubmit={handleLogin}>
								<div className="mb-6"><span>
									<div>
										<div className="relative">
											<input
												onChange={(e) => setOrgName(e.target.value)}
												id="orgname"
												type="text"
												value={orgName}
												name="orgname"
												placeholder="Organization Name *"
												required="required"
												className="border border-gray-300 text-gray-900 sm:text-sm rounded-md outline-none block w-full p-3 focus:ring-blue-500 focus:border-blue-500" />
										</div>
									</div>
								</span></div>
								<div className="mb-6"><span>
									<div>
										<div className="relative">
											<input
												onChange={(e) => setUserName(e.target.value)}
												id="username"
												type="text"
												value={userName}
												name="username"
												placeholder="Username *"
												required="required"
												className="border border-gray-300 text-gray-900 sm:text-sm rounded-md outline-none block w-full p-3 focus:ring-blue-500 focus:border-blue-500" />
										</div>
									</div>
								</span></div>
								<div className="mb-6"><span>
									<div>
										<div className="relative">
											<input
												onChange={(e) => setPassword(e.target.value)}
												id="password"
												type={showPassword ? 'text' : 'password'}
												value={password}
												name="password"
												placeholder="Password *" required="required"
												className="border border-gray-300 text-gray-900 sm:text-sm rounded-md outline-none block w-full p-3 focus:ring-blue-500 focus:border-blue-500" />
											<div onClick={() => setShowPassword(e => !e)}>
												<svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" role="img"
													preserveAspectRatio="xMidYMid meet" viewBox="0 0 16 16"
													className="cursor-pointer w-5 h-5 absolute top-1/2 transform -translate-y-1/2 right-3">
													<path fill="currentColor"
														d="M8 6.003a2.667 2.667 0 1 1 0 5.334a2.667 2.667 0 0 1 0-5.334Zm0 1a1.667 1.667 0 1 0 0 3.334a1.667 1.667 0 0 0 0-3.334Zm0-3.336c3.076 0 5.73 2.1 6.467 5.043a.5.5 0 1 1-.97.242a5.67 5.67 0 0 0-10.995.004a.5.5 0 0 1-.97-.243A6.669 6.669 0 0 1 8 3.667Z">
													</path>
												</svg>
											</div>
										</div>
									</div>
								</span></div>
								<div className="flex items-start mb-6">
								</div>
								<button type="submit"
									className="font-medium rounded-md px-5 border py-2.5 text-center bg-primary-main bg-blue-900 text-white focus:ring-blue-300 opacity-100 w-full">
									Login
								</button>
							</form>
						</span>
						<p className="mt-5 text-sm text-center">
							Not registered yet?
							<a href="/#" className="text-primary-main hover:underline font-medium mx-1">
								Contact us
							</a>
							for more info
						</p>
					</div>
					<div className="flex items-center ml-6">
						<img className="w-80" src="https://taskdev.mile.app/591c9afae08d42365820ece5754e7bfc.png" />
					</div>
				</div>
			</div>
		</div>
	)
}

export default Login