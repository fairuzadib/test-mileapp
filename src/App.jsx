import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import routes from './config/routes';

const App = () => {
    return (
        <BrowserRouter>
            <Routes>
              {
                routes.map(({ path, Component }) => {
                    return (
                      <Route
                          key={path}
                          path={path}
                          element={(
                            <Component />
                          )}
                      />
                    );
                })
              }
            </Routes>
        </BrowserRouter>
    );
};

export default App;