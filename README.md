# Demo
https://luminous-starship-c6d6b4.netlify.app/login


# Jawaban Test

1. jawaban singkatnya adalah saya memilih menggunakan reactjs karena masalah selera. namun saya bisa handle keduanya.
2. melakukan migrasi dari CSR menjadi SSR dengan membuat custom backend tanpa menggunakan framework SSR
3. sebagai frontend developer harus punya sensitivitas terhadap UX karena akan berdampak pada kenyamanan pengguna saat mengakses sebuah web
4.  - dari segi pewarnaan pada form login agak samar dengan background
    - mungkin lebih baik organization name nya dibuat sekaligus request ke server berbarengan dengan email dan password.
    - kalau ingin tetap dipisah mungkin lebih baik inputan pada email dan password tidak perlu disabled, cukup button loginnya saja yg disabled. karena saat menekan tab fokusnya jadi kemana2
    - tambahkan show/hide password
5. in this repo
6. 
    a.  
    ```javascript
    let a = 3
    let b = 5

    a = a + b
    b = a - b
    a = a - b
    ```
    b.
    ```javascript
    const findMissingNumber = (arr) => {
        const temp = []
        let n = 0
        for (let i = 0; i < arr.length; i++) {
            if(arr[n] !== i+1){
                temp.push(i+1)
            } else {
                n++
            }
        }
        return temp
    }
    ```
    c.
    ```javascript
    const findMultipleNumber = (arr) => {
        const obj = {}
        const temp = []
        for (let i = 0; i < arr.length; i++) {
            let n = arr[i]
            if(obj[n] === undefined){
                obj[n] = 0
            }
            obj[n] += 1
            if(obj[n] === 2){
                temp.push(n)
            }
        }
        return temp
    }
    ```
    d.
    ```javascript
    let arr = ["1.", "1.1.", "1.2.", "1.3.", "1.4.", "1.1.1.", "1.1.2.", "1.1.3.", "1.2.1.", "1.2.2.", "1.3.1.", "1.3.2.", "1.3.3.", "1.3.4.", "1.4.1.", "1.4.3."]

    const convertToTree = (arr) => {
        let obj = {}
        for (let i = 0; i < arr.length; i++) {
            const n1 = arr[i];
            let temp = obj
            const splitted = n1.split('.')
            for (let j = 0; j < splitted.length; j++) {
                const n2 = splitted[j];
                if (n2) {
                    if (splitted.length === j+1 || !splitted[j+1]) {
                        temp[n2] = n1
                    } else {
                        if (typeof temp[n2] === 'string') {
                            temp[n2] = {}
                        }
                        temp[n2] = {
                            ...temp[n2]
                        }
                    }
                    temp = temp[n2]
                }
            }
        }
        return obj
    }
    ```